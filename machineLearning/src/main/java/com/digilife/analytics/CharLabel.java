package com.digilife.analytics;

import org.intelligentjava.machinelearning.decisiontree.label.Label;

/**
 * String labels, such as good. bad and average
 * 
 * @author Adnan Khan
 *
 */
public class CharLabel extends Label {
	
	public static final char BAD_CHAR = 'b';
	public static final char GOOD_CHAR = 'g';
	public static final char AVERAGE_CHAR = 'a';
	
	public static final String BAD = "bad";
	public static final String GOOD = "good";
	public static final String AVERAGE = "average";
    
    public static final Label GOOD_LABEL = CharLabel.newLabel(GOOD_CHAR);
    public static final Label BAD_LABEL = CharLabel.newLabel(BAD_CHAR);
    public static final Label AVERAGE_LABEL = CharLabel.newLabel(AVERAGE_CHAR);
    
    /** Label. */
    private char label;
    
    /**
     * Constructor.
     */
    private CharLabel(char label) {
        super();
        this.label = label;
    }
    
    /**
     * {@inheritDoc}
     */
    @Override
    public String getName() {
        if(label==GOOD_CHAR){
        	return GOOD;
        }
        if(label==BAD_CHAR){
        	return BAD;
        }
        return AVERAGE;
    }
    
    /**
     * Static factory method.
     */
    public static Label newLabel(char label) {
        return new CharLabel(label);
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "CharLabel [label=" + label + "]";
    }

    /**
     * {@inheritDoc}
     */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + label;
		return result;
	}

	 /**
     * {@inheritDoc}
     */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CharLabel other = (CharLabel) obj;
		if (label != other.label)
			return false;
		return true;
	}

	@Override
	public String getPrintValue() {
		return getName();
	}
}
