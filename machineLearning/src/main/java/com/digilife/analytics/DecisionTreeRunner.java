package com.digilife.analytics;

import static org.intelligentjava.machinelearning.decisiontree.feature.P.betweenD;
import static org.intelligentjava.machinelearning.decisiontree.feature.P.lessThanD;
import static org.intelligentjava.machinelearning.decisiontree.feature.P.moreThanD;
import static org.intelligentjava.machinelearning.decisiontree.feature.PredicateFeature.newFeature;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;

import org.intelligentjava.kaggle.titanic.Main;
import org.intelligentjava.machinelearning.decisiontree.DecisionTree;
import org.intelligentjava.machinelearning.decisiontree.data.DataSample;
import org.intelligentjava.machinelearning.decisiontree.data.SimpleDataSample;
import org.intelligentjava.machinelearning.decisiontree.feature.Feature;

import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseChar;
import org.supercsv.cellprocessor.ParseDouble;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvListReader;
import org.supercsv.io.ICsvListReader;
import org.supercsv.prefs.CsvPreference;
import org.supercsv.util.CsvContext;

import com.google.common.collect.Lists;

public class DecisionTreeRunner {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		List<DataSample> trainingData = readData(true);
		DecisionTree tree = new DecisionTree();

		List<Feature> features = getFeatures();

		tree.train(trainingData, features);

		// print tree after training
		tree.printTree();

		// read test data
		List<DataSample> testingData = readData(false);
		List<String> predictions = Lists.newArrayList();
		// classify all test data
		for (DataSample dataSample : testingData) {
			predictions.add(dataSample.getValue("Name").get() + "," + tree.classify(dataSample).getPrintValue());
		}

		// write predictions to file
		FileWriter fileWriter = new FileWriter(new File("predictions.csv"));
		fileWriter.append("Name,ActivityLevel").append("\n");
		for (String prediction : predictions) {
			fileWriter.append(prediction).append("\n");
		}
		fileWriter.flush();
		fileWriter.close();

	}

	private static List<Feature> getFeatures() {

		Feature dailyStepsLessThan1K = newFeature("DailyStepsAvg", lessThanD(1000.0), "less than 1K");
		Feature dailyStepsBetween1KAnd6K = newFeature("DailyStepsAvg", betweenD(1000.0, 6000.0), "between 1K and 6K");
		Feature dailyStepsBetween6KAnd12K = newFeature("DailyStepsAvg", betweenD(6000.0, 12000.0),"between 6K and 12K");
		Feature dailyStepsBetween12KAnd18K = newFeature("DailyStepsAvg", betweenD(12000.0, 18000.0),"between 12K and 18K");
		Feature dailyStepsMoreThan18K = newFeature("DailyStepsAvg", moreThanD(18000.0), "more than 18K");

		Feature isMale = newFeature("Sex", "male");
		Feature isFemale = newFeature("Sex", "female");

		Feature bmiLessThan17 = newFeature("Bmi", lessThanD(17.0), "less than 17");
		Feature bmiBetween17And25 = newFeature("Bmi", betweenD(17.0, 25.0), "between 17 and 25");
		Feature bmiBetween25And30 = newFeature("Bmi", betweenD(25.0, 30.0), "between 25 and 30");
		Feature bmiMoreThan30 = newFeature("Bmi", moreThanD(30.0), "more than 30");

		Feature ageLessThan15 = newFeature("Age", lessThanD(15.0), "less than 15");
		Feature ageBewteen15And30 = newFeature("Age", betweenD(15.0, 30.0), "between 15 and 30");
		Feature ageBewteen30And50 = newFeature("Age", betweenD(30.0, 50.0), "between 30 and 50");
		Feature ageBewteen50And60 = newFeature("Age", betweenD(50.0, 60.0), "between 50 and 60");
		Feature ageMoreThan60 = newFeature("Age", moreThanD(60.0), "more than 60");

		return Arrays.asList(dailyStepsLessThan1K, dailyStepsBetween1KAnd6K, dailyStepsBetween6KAnd12K,
				dailyStepsBetween12KAnd18K, dailyStepsMoreThan18K, isMale, isFemale, ageLessThan15, ageBewteen15And30,
				ageBewteen30And50, ageBewteen50And60, ageMoreThan60, bmiLessThan17, bmiBetween17And25,
				bmiBetween25And30, bmiMoreThan30);
	}

	private static List<DataSample> readData(boolean training) throws IOException {
		List<DataSample> data = Lists.newArrayList();
		String filename = training ? "/train.csv" : "/test.csv";
		InputStreamReader stream = new InputStreamReader(DecisionTree.class.getResourceAsStream(filename));
		/*ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource("file/test.xml").getFile());*/
		try (ICsvListReader listReader = new CsvListReader(stream, CsvPreference.STANDARD_PREFERENCE);) {

			// the header elements are used to map the values to the bean (names
			// must match)
			final String[] header = listReader.getHeader(true);

			List<Object> values;
			while ((values = listReader.read(getProcessors(training))) != null) {
				data.add(SimpleDataSample.newSimpleDataSample("Category", header, values.toArray()));
			}
		}
		return data;
	}
	
	private static CellProcessor[] getProcessors(boolean training) {
		if (training) {
			final CellProcessor[] processors = new CellProcessor[] { 
					new Optional(), 
					new Optional(new ParseDouble()),
					new Optional(), 
					new Optional(new ParseDouble()), 
					new Optional(new ParseDouble()),
					new Optional(new ParseCharLabel()) 
				};
			return processors;
		} else {
			final CellProcessor[] processors = new CellProcessor[] { 
					new Optional(), 
					new Optional(new ParseDouble()),
					new Optional(), 
					new Optional(new ParseDouble()), 
					new Optional(new ParseDouble()) 
				};
			return processors;
		}
	}

	private static class ParseCharLabel extends ParseChar {

		public Object execute(final Object value, final CsvContext context) {
			char parsed = (Character) super.execute(value, context);
			if (parsed == CharLabel.GOOD_CHAR) {
				return CharLabel.GOOD_LABEL;
			}
			if (parsed == CharLabel.BAD_CHAR) {
				return CharLabel.BAD_LABEL;
			}
			return CharLabel.AVERAGE_LABEL;
		}
	}
}
